#!/bin/bash

set -e;

cd general
dub --build=release -- -i ../keywords.js --time
dub --build=release -- -i ../issue1.js --time
dub --build=release -- -i ../issue2.js --time
dub --build=release -- -i ../react.js --time
cd ..